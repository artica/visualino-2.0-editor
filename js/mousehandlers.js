		
		var mousedown = false;
		
		function mouseDown(event)
		{
			if (!c) return;
			
			var x = new Number();
        	var y = new Number();
			if (event.x != undefined && event.y != undefined)
			{
			  x = event.x;
			  y = event.y;
			}
			else // Firefox method to get the position
			{
			  x = event.clientX + document.body.scrollLeft +
				  document.documentElement.scrollLeft;
			  y = event.clientY + document.body.scrollTop +
				  document.documentElement.scrollTop;
			}

			//var c = document.getElementById("mycanvas");
		  	
		  	x -= c.offsetLeft;
		  	y -= c.offsetTop;
		  			  	
			mousedown = true;
		  	g.mouseDown(x,y);

		} 

		function mouseUp(event)
		{
			if (!c) return;
		
			var x = new Number();
        	var y = new Number();
			if (event.x != undefined && event.y != undefined)
			{
			  x = event.x;
			  y = event.y;
			}
			else // Firefox method to get the position
			{
			  x = event.clientX + document.body.scrollLeft +
				  document.documentElement.scrollLeft;
			  y = event.clientY + document.body.scrollTop +
				  document.documentElement.scrollTop;
			}

			//var c = document.getElementById("mycanvas");
		  	
		  	x -= c.offsetLeft;
		  	y -= c.offsetTop;
		  	
		  	mousedown = false;
		  	g.mouseUp(x,y);

		} 

		function mouseMove(event)
		{
			if (!c) return;
			
			var x = new Number();
        	var y = new Number();
			if (event.x != undefined && event.y != undefined)
			{
			  x = event.x;
			  y = event.y;
			}
			else // Firefox method to get the position
			{
			  x = event.clientX + document.body.scrollLeft +
				  document.documentElement.scrollLeft;
			  y = event.clientY + document.body.scrollTop +
				  document.documentElement.scrollTop;
			}

			//var c = document.getElementById("mycanvas");
		  	
		  	x -= c.offsetLeft;
		  	y -= c.offsetTop;
		  			  	
		  	if (mousedown) {
		  		g.mouseDrag(x,y);
		  	} else {
		  		g.mouseMove(x,y);
			}
			
		} 
