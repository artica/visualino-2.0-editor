
// Threshold to assume clicks on the connections between nodes
// The value represents how far away we can click from the line
// to still consider the click close enough to select it
var CONNECTION_CLICK_THRESHOLD = 12;


function Graph(node_class) 
{
	this.node_class = node_class;
	
	this.reset_graph();
	
	this.s = new SourceColumn();
	this.uid_function = new UID(0);
	
	this.tempx = 0;
	this.tempy = 0;
	this.tempconnect = false;
	
	this.panCanvas = false;
	this.dragStartX = 0;
	this.dragStarty = 0;
	
	this.corner_x = 0;
	this.corner_y = 0;

	this.screen_zoom = 1.0;

	// @todo fix this variable (not working well since the canvas was expanded)
	this.inputBoxTyping = false;
	
	this.selectedNode = -1; // used for refreshing properties div

	// Last node that was selected
	this.lastSelectedNode = -1;
	
	// Flag used when any element is moved. Setting this flag to true
	// makes the worker thread check if any repositioning is necessary
	this.tree_moved = false;
	
	// Does the tree need to be updated because of collisions?
	this.update_collision = true;
	// Does the tree need to be updated because of animations?
	this.update_animation = true;
	
	
	this.importing = false;
}

Graph.prototype = 
{

	//
	// node handling
	//
	
	reset_graph: function() 
	{
		this.root = null;
		this.nodes = [];
		this.num_nodes = 0;
		
		if (this.collision_worker) 
		{
			this.collision_worker.terminate();
		}
		this.collision_worker = new Worker('js/graph-upd_node_pos-worker.js');
		this.collision_worker.onmessage = this.update_node_positions;
		this.update_collision = true;

	},
	add_node: function (name, type, x, y, radius) 
	{
		var v = new this.node_class(name, type);

		if ((x !== undefined) && (y !== undefined)) 
		{
			v.set_position(x,y);
		}
		if (radius !== undefined) {
			v.set_radius(radius);
		}
		
		this.nodes[this.nodes.length] = v;

		this.num_nodes++;

		return v;
	},
	remove_node: function (v) 
	{
		// Make sure it actually exists
		if (!(v in this.nodes)) return null;

		// If the node has a parent, remove the link from the parent to this node
		if (this.nodes[v].parent)
			this.nodes[v].parent.remove_child(this.nodes[v]);
		
		// If this node has children, remove the parent on all of then
		for (var i=0; i<this.nodes[v].children.length; i++)
		{
			this.nodes[v].children[i].parent = null;
		}

		// All the links have been taken care of, so we can remove the node safely
		this.nodes.splice(v, 1);		
		this.num_nodes--;
		
		// refresh properties display
		this.clear_display();
		
		return v;
	},
	remove_selected_nodes: function(deleteThis) 
	{
		if (!this.inputBoxTyping || deleteThis) 
		{
			for (var i = this.nodes.length-1; i >= 0; i--) 
			{
				if(this.nodes[i].selected) 
				{
					this.remove_node(i);
				}
			}
		}
	},

	remove_selected_connections: function(deleteThis) 
	{
		if (!this.inputBoxTyping || deleteThis) 
		{
			for (var i = 0; i < this.nodes.length; i++) 
			{
				this.nodes[i].connection_remove();
			}
		}
	},
	
	remove_selection: function(deleteThis) 
	{
		this.remove_selected_connections(deleteThis);
		this.remove_selected_nodes(deleteThis);
		startAnimation();

	},

	// Sets a new state for a specific node
	set_node_state: function (node, state)
	{
		//console.log('set_node_state:'+node+','+state);
		//console.log(this.nodes[node]);
		this.nodes[node].set_state(state);
	},

	//
	// update node positions
	//
	// need to use "g." instead of "this." because it's a listener function 
	update_node_positions: function(e) 
	{	
		console.log('e.data.updated:'+e.data.updated);
		g.update_collision = e.data.updated;
		
		if (mousedown) 
		{
			return;
		} 
		else 
		{
			for (var id in e.data.nodes) 
			{
				var thisnode = e.data.nodes[id];
				for (var i = 0; i < g.nodes.length; i++) 
				{
					if(g.nodes[i].uid == thisnode['uid']) 
					{
						g.nodes[i].set_position(thisnode['x'], thisnode['y']);
					}
				}		
			}
		}
	},

	function zoomTree(zoom_offset)
	{
		// Calculate the position of the cursor relative to the 

	        g.zoom(0.1);
	        g.screen_zoom += 0.1;
	        if (g.screen_zoom > 10.0) g.screen_zoom = 10.0;
	    }
	    else
	    {
	        console.log('scrolling down !');
	        g.screen_zoom -= 0.1;
	        if (g.screen_zoom < 0.1) g.screen_zoom = 0.1;

	    }
		startAnimation();        
	}

	
	//
	// find root
	//
	
	// Finds the root of the tree
	// @todo update this function
	find_root: function() 
	{
		for (var i=0; i<this.nodes.length; i++) 
		{
			if (this.nodes[i]['type']['name'] == 'Root') 
			{
				return i;
			}
		}
		return -1;
	},
	
	//
	// drawing
	//
	
	draw: function (ctx) 
	{
		//console.log('Yoink! '+this.update_collision+'  '+this.update_animation);
		// update nodes positions with a webworker
		if (this.update_collision) 
		{
			var mynodes = [];
			for (var j = 0; j < this.nodes.length; j++) 
			{
				mynodes[j] = [];
				mynodes[j]['uid'] = this.nodes[j]['uid'];
				mynodes[j]['x'] = this.nodes[j]['x'];
				mynodes[j]['y'] = this.nodes[j]['y'];
			}
			this.collision_worker.postMessage({'nodes': mynodes});
		}
		
		// clear canvas background
		ctx.clearRect(0,0,cw,ch);
		
		// draw source type selectors
		var sy = this.s.sources_vpad;
		for (m = 0; m < this.s.sourcetypes.length; m++) 
		{
			var sx = this.s.sources_hpad*0.75 + m*this.s.sources_hpad*0.75;
			
			drawCircle( ctx, sx, sy, this.s.sources_height*0.5,
						this.s.sourcetypes[m]['color'],
						"transparent"
					);
		}
		
		// draw source selectors
		this.s.draw(ctx);


		// Animate the nodes (expanding/collapsing the tree)
		this.update_animation = false;
		for (var i = 0; i < this.nodes.length; i++) 
		{
			if (this.nodes[i].update_animation()) this.update_animation = true;
		}
		//console.log('this.update_animation:'+this.update_animation)
		
		if (!this.update_collision && !this.update_animation) 
		{
			//console.log('-------------------------------------le cancel!!!');
			cancelAnimationFrame(animation_frame);
		}
		
		
		// draw connections between nodes
		for (var i = 0; i < this.nodes.length; i++) 
		{
			this.nodes[i].draw_connections(ctx);
		}
		
		//draw nodes
		for (var i = 0; i < this.nodes.length; i++) 
		{
			//console.log(this.nodes[i]);
			this.nodes[i].draw(ctx);
		}
		
		if (this.s.source_selected != -1) this.draw_temp();
		
	},
	
	//
	// mouse handling
	//
	
	mouseDown: function (x,y) 
	{
		//var pd = document.getElementById('propertiesdiv');
		this.dragStartX = x;
		this.dragStartY = y;


		console.log('mouseDown: x:'+x+'  y:'+y);

		// process drags on the canvas, not the display properties box
		//if( x < pd.offsetLeft)
		{
			// unselect every node if the control key is not pressed
			if (!ctrlDown)
			{
				for (var i = 0; i < this.nodes.length; i++) 
				{	
					this.nodes[i].unselect();
					this.nodes[i].connection_unselect();
					
				}
			}
			
			this.inputBoxTyping = false;
		
			// check if it's hitting any node
			var hit_node = false;
			for (var i = 0; i < this.nodes.length; i++)
			{
				// Don't bother checking nodes that are not visible
				if (this.nodes[i].visible === false) continue;
				// Check if the click was inside the radius of the node body
				if (distance_to_point(x,y,this.nodes[i].display_x,this.nodes[i].display_y) < this.nodes[i].radius) 
				{
					if (shiftDown) this.nodes[i].select_tree();
					else if (ctrlDown) this.nodes[i].toggle_selection();
					else this.nodes[i].select();
					hit_node = true;
					
					this.selectedNode = i;
					this.lastSelectedNode = i;
					
					console.log(this.nodes[i]);
					break;
				}
				// Check if the click was inside the radius of the expand/collapse button
				else if (distance_to_point(x,y,this.nodes[i].display_x+this.nodes[i].radius+
					COLLAPSE_BUTTON_SIZE, this.nodes[i].display_y) < COLLAPSE_BUTTON_SIZE) 
				{
				
					hit_node = true;
					// Check if the node is currently expanded or collapsed and toggles it
					if (this.nodes[i].expanded)	this.nodes[i].collapse();
					else this.nodes[i].expand();
					console.log(this.nodes[i]);
					break;
				}
				
			}
		
			// didnt hit any node
			if (!hit_node) 
			{		
				// check if it's hitting any source type selectors
				var hit_source = false;				
				var sy = this.s.sources_vpad;
				for (m = 0; m < this.s.sourcetypes.length; m++) 
				{
					var sx = this.s.sources_hpad*0.75 + m*this.s.sources_hpad*0.75;
					if (distance_to_point(x,y,sx,sy) < this.s.sources_height*0.5) 
					{
						this.s.change_sourcetype(m);
						hit_source = true;
					}
				}
			
				// check if it's hitting any source
				for (var s = 0; s < this.s.sourcetypes[this.s.sourcetype_selected]['nodes'].length; s++) 
				{
					var thisx = this.s.sources_hpad;
					var thisy = this.s.sources_top + this.s.sources_vpad + s*(this.s.sources_height+this.s.sources_vpad);
					if (distance_to_point(x,y,thisx,thisy) < this.s.sources_radius) 
					{
						this.s.source_selected = s;
						this.update_temp(x,y);
						hit_source = true;
					}
				}				
				this.clear_display();
			}
			
			// If we didn't hit a node or a source, let's check the connections
			var hit_connection = false;				
			if (!hit_node && !hit_source) 
			{
				// Go through all the nodes...
				for (var i = 0; i < this.nodes.length; i++)
				{
					// For each node, check all the child connections
					for (var j = 0; j < this.nodes[i].children.length; j++)
					{
						// Check if the click is close enough the the line segment between the two nodes
						if (distToSegment({x:x, y:y}, this.nodes[i], this.nodes[i].children[j]) < CONNECTION_CLICK_THRESHOLD)
						{
							this.nodes[i].connection_select(this.nodes[i].children[j]);
							//console.log('connection_select:'+j );
							hit_connection = true;
							break;
						}

					}
					// If we already hit a connection, no need to check any other nodes
					if (hit_connection) break; 
				}
			}
			
			
			if (!hit_node && !hit_source && !hit_connection) 
			{
				this.panCanvas = true;
			}						
		}

		startAnimation();
	},
	mouseUp: function (x,y) 
	{
		startAnimation();

		g.update_collision = true;

		//console.log('mouseUp: source_selected:'+this.s.source_selected+'  selectedNode:'+this.selectedNode);
		var moved_node = null;
		// Check if we were dragging a new node
		if (this.s.source_selected != -1) 
		{
			console.log('New node!');
			var srctype = this.s.sourcetypes[this.s.sourcetype_selected]['nodes'][this.s.source_selected];
			
			// check if any unique nodes are being duplicated
			var skip = false;
			if (srctype.unique == true) 
			{
				for (var i = 0; i < this.nodes.length; i++) 
				{
					if ( this.nodes[i].type.name == srctype.name)
					{
						skip = true;
						break;
					}
				}
			}
			// A duplicate unique node was found, won't create this one
			if (skip) 
			{
				alert(t[lang]['duplicate']);
			}
			// Otherwise all is good, let's create the new node
			else
			{
				moved_node = g.add_node('',	deepCopy(srctype).init(), x, y, 20);
			}
		}
		// Check if we were dragging an existing node with no parent
		else if (this.selectedNode != -1)
		{
			moved_node = this.nodes[this.selectedNode];
		}
		
		// If we were dragging a node (either a new or an existing one) with no parent, 
		// let's see if it landed on top of another existing node
		if (moved_node != null && moved_node.parent === null)
		{
			//console.log("BING");
			
			for (var i = 0; i < this.nodes.length; i++)
			{		
				// Compare the node position with all the other nodes 
				if (this.nodes[i] !==  moved_node &&
					distance_to_point(x,y,this.nodes[i].x,this.nodes[i].y) < this.nodes[i].radius*2) 
				{
					// don't allow to add more nodes to leaf nodes 
					if (this.nodes[i].type.leaf === true) 
					{
						alert(t[lang]['error_leaf']);
						break;
					}
					
					// don't allow a node to be a child of a descendant
					// (this would create a closed loop)
					if (this.nodes[i].check_ancestors(moved_node)) 
					{
						alert(t[lang]['error_loop']);
						break;
					}
					
					moved_node.y += 60;
					// Adds the new node as a child of the existing node where it was just dropped
					this.nodes[i].add_child(moved_node);
				
					this.s.unselect_source();
				}
			}
		}
		
		else 
		{
			//console.log("NHEC");			
			// check if its releasing a selected node
			for (var i = 0; i < this.nodes.length; i++) {	
				if ( ( distance_to_point(x,y,this.nodes[i].x,this.nodes[i].y) < this.nodes[i].radius) &&
					 ( this.nodes[i].selected ) )
				{
					//console.log('display this nodes info');
					this.selectedNode = i;
					this.lastSelectedNode = i;
					this.display_node_properties(i);
				}
			}	
			
		}
		
		this.s.source_selected = -1;
		this.selectedNode = -1;

		this.panCanvas = false;
		this.tree_moved = true;
		startAnimation();
	},
	mouseMove: function (x,y) 
	{
		// do nothing
	},
	mouseDrag: function (x,y) 
	{
		var pd = document.getElementById('propertiesdiv');

		var offset_x = this.dragStartX - x;
		var offset_y = this.dragStartY - y;
		
		this.dragStartX = x;
		this.dragStartY = y;

		// process drags on the canvas, not on the display properties box
//		if( x < pd.offsetLeft) 
		{
			var s = this.s.source_selected;
			if (s != -1) 
			{
				// dragging from a source, draw a temporary node
				this.update_temp(x,y);
			} else 
			{				
				if (!this.inputBoxTyping) 
				{
					for (var i = 0; i < this.nodes.length; i++) 
					{
						if(this.nodes[i].selected) 
						{								
							this.nodes[i].move_node(offset_x, offset_y);
						}
					}
				}
						
			}
			
			if (this.panCanvas) 
			{
				
				for (var i = 0; i < this.nodes.length; i++) 
				{
					this.nodes[i].move_node(offset_x, offset_y);
				}
			}
			
		}
		startAnimation();
	},
	
	//
	// temporary node
	//
	
	update_temp: function (x,y) 
	{
		this.tempx = x;
		this.tempy = y;
	},
	
	draw_temp: function () 
	{
		ctx.fillStyle = this.s.sourcetypes[this.s.sourcetype_selected]['color'];
		ctx.beginPath();
		ctx.arc( this.tempx, this.tempy, this.s.sources_height, 0, Math.PI*2, true );
		ctx.fill();
		ctx.closePath();
	},
	
	//
	// properties display box
	//
	
	display_node_properties: function(i) 
	{
		var dom = document.getElementById('propertiesdisplaybox');
		if (dom) {

			var output = '';
			
			// input box to delete this node
			output += '<br><input type="button" onclick="g.remove_selected_nodes(true);" value="'+
				t[lang]['delete_node']+'" /><br>';
			
			// properties table
			output += '<table>';
			
			// add uid, unchangable
			output += '<tr><td>'+t[lang]['UID']+'</td><td>'+this.nodes[i].uid+'</td></tr>';
			
			// add name, editable textbox
			output += '<tr><td>'+t[lang]['Name']+'</td><td><input id="nameInput" name="nameInput" value="'+
				this.nodes[i].name+'"/></td></tr>';
			
			// node-specific properties
			output += this.nodes[i].type.list_properties();

			output += '</table>';
			
			dom.innerHTML = output;
			
			// add listener for auto-update of name
			var nameInputBox = document.getElementById('nameInput');
			nameInputBox.onkeyup = function() {
				g.inputBoxTyping = true;
				g.nodes[i].name = nameInputBox.value;
			}
			this.nodes[i].type.add_list_properties_listeners();
		}
		
		var infobox = document.getElementById('infobox');
		if (infobox) {
			var inforef = this.nodes[i].type.name + ' Info';
			infobox.innerHTML = t[lang][inforef] + '<br><br>' + t[lang]['del'];
		}		
	},
	
	clear_display: function() 
	{
		var dom = document.getElementById('propertiesdisplaybox');
		if (dom) 
		{

			var html = "<input id='fileToLoad' type='file' value='test'></input><br><br\>"+
				"<button id='button_export_json'>"+ t[lang]['Export .json']+ "</button><br><br\>"+
				"<table><tr>"+
				"<td><img id='button_upload_arduino' class='control_buton' src='images/arduino.png'></img></td>"+
				"<td><img id='button_save_eeprom' class='control_buton' src='images/chip.png'></img></td>"+
				"<td><img id='button_run_tree' class='control_buton' src='images/play.png'></img></td>"+
				"<td><img id='button_stop_tree' class='control_buton' src='images/stop.png'></img></td>"+
				"<td><img id='button_pause_tree' class='control_buton' src='images/pause.png'></img></td>"+
				"<td><img id='button_step_tree' class='control_buton' src='images/step.png'></img></td>"+
				"</tr></table>";
			$('#propertiesdisplaybox').html(html);

			$('#fileToLoad').on('change', this.load_from_file);
			$('#button_export_json').click(this.export_to_json);
			$('#button_upload_arduino').click(this.upload_to_arduino);

			$('#button_save_eeprom').click(function() {sendData('E;');});
			$('#button_run_tree').click(function() {sendData('R;');});
			$('#button_stop_tree').click(function() {sendData('S;');});
			$('#button_pause_tree').click(function() {sendData('P;');});
			$('#button_step_tree').click(function() {sendData('F;');});

			


			/*
			dom.innerHTML = '';
	
			var dom_bt = document.createElement("button");
			dom_bt.onclick = this.export_to_json;
			dom_bt.innerHTML = t[lang]['Export .json'];
			dom.appendChild(dom_bt);
			
			var dom_br = document.createElement("br");
			dom.appendChild(dom_br);
			
			var dom_br2 = document.createElement("br");
			dom.appendChild(dom_br2);
			
			var dom_bt2 = document.createElement("button");
			dom_bt2.onclick = this.upload_to_arduino;
			dom_bt2.innerHTML = t[lang]['Export .ino'];
			dom.appendChild(dom_bt2);
			
			var dom_br3 = document.createElement("br");
			dom.appendChild(dom_br3);
			
			var dom_br4 = document.createElement("br");
			dom.appendChild(dom_br4);
			
			var dom_in = document.createElement("input");
			dom_in.setAttribute("id", "fileToLoad");
			dom_in.setAttribute("type", "file");
			dom_in.onchange = this.load_from_file;
			dom.appendChild(dom_in);
			*/
			
		}
		
		var infobox = document.getElementById('infobox');
		if (infobox) 
		{
			infobox.innerHTML = t[lang]['infobox_default'] + '<br><br>' + t[lang]['del'];
		}
	},
	target_platform_changed: function() 
	{
		// get root targets reference
		for (var i=0; i<this.nodes.length; i++) {
			output.push( this.nodes[i].export_node() );
		}
		
		// if a node is of a type listed in root targets,
		// and is using that template, update it
		for (var i=0; i<this.nodes.length; i++) {
			// if has template
			// and if has same type as listed in one of the root targets
			// and the name is the same
			
			//todo: change the type.targeted
			
			//todo: finish this
		}
	},

	//
	// import / export
	//
	
	// Re-order all nodes so the order matches the visual appearance
	sort_nodes: function()
	{
		var root = this.find_root();
		this.nodes[root].sort_children(0);
		this.nodes.sort(function(node1, node2) 
			{
				return node1.id - node2.id;
			});
		
	},
	
	export_nodes: function() 
	{
		var output = [];
		for (var i=0; i<this.nodes.length; i++) 
		{
			output.push( this.nodes[i].export_node() );
		}
		return output;
	},
	
	export_connections: function() 
	{
		var output = [];
		for (var i=0; i<this.nodes.length; i++) 
		{
			console.log('node:'+i);
			console.log(this.nodes[i]);
			
			for (var j=0; j<this.nodes[i].children.length; j++)
			{		
				console.log('exporting:'+i+','+j);
				output.push( this.nodes[i].export_connection(j) );
			}
		}
		return output;				
	},
	
	export_json: function() {
		this.exportjson = {
			"nodes": this.export_nodes(),
			"connections": this.export_connections()
		};
		
		this.jsonstring = JSON.stringify(this.exportjson);
		
		return this.jsonstring;
	},
	export_arduino: function() 
	{
		this.sort_nodes();
		
		// header
		var output = 'L;T05'+padByte(this.nodes.length)+';D000103;D010105;D020106;D030308;';
		
		//L;N00010201;N010202;N020202;N030004;N0403100001;N05030000000088;N0603100000;N07030000000099;N080004;N0903100101;N0A0300000002FF;N0B03100100;N0C0300000003A6;C0001;C0002;C0103;C0304;C0305;C0306;C0307;C0208;C0809;C080A;C080B;C080C;F;
				
		// list all nodes
		var rootnode = this.find_root();

		for (var i=0; i<this.nodes.length; i++) 
		{
			output += this.nodes[i].export_arduino();
		}
		for (var i=0; i<this.nodes.length; i++) 
		{
			output += this.nodes[i].export_connections();
		}
		
		output += 'F;';
		
		console.log(output);
		// list all connections
		// @todo fazer isto com a arvore
		/*
		for (var i=0; i<this.connections.length; i++) {
		
			// follows top to bottom convention, bottom node is child of top node
			
			var parent = this.connections[i].c1;
			var child = this.connections[i].c2;
			
			if (this.connections[i].c1.y > this.connections[i].c2.y) {
				parent = this.connections[i].c2;
				child = this.connections[i].c1;
			}
			
			var np = nodeprefix + parent.uid;
			var nc = nodeprefix + child.uid;
			output += '\
	' + np + '->addChild(' + nc + ');\n';
			
		}
		*/

		return output;
	},
	
	// Debug function to reset the node grow animation
	reset_animation: function(value)
	{
		for (var i = 0; i < this.nodes.length; i++) 
		{
			this.nodes[i].reset_animation(value);
		}
		
	},
	
	// Copy the current nodes to a temporary buffer
	copySelected: function()
	{
		console.log("copySelected");
		for (var i = 0; i < this.nodes.length; i++) 
		{
			//this.nodes[i].reset_animation(value);
		}
		
	},

	// Paste the nodes from temporary buffer to the current screen
	pasteSelected: function()
	{
		console.log("pasteSelected");
		for (var i = 0; i < this.nodes.length; i++) 
		{
			//this.nodes[i].reset_animation(value);
		}
		
	},

	// Imports a tree from a json string
	// if a parent is specified, the first node of the imported tree
	// (which should be its root) is added as a child of the parent node
	import_json: function(jsonstring, parent) 
	{
		console.log('import_json:'+jsonstring);
		console.log('parent:');
		console.log(parent);
		var temp = JSON.parse(jsonstring);

		// Offset to apply to all of the loaded nodes (when a parent exists)
		// The offset is calculated so that the loaded tree stays below the parent node
		var x_offset = 0;
		var y_offset = 0;
		
		// variable with associations between the old and new ids for the new nodes
		// (they have specific id's in the json string, but will get new ones now)
		var node_convert = {};
		
		var root_node = true;
		// import nodes
		var highest_uid = 0;
		for (var i=0; i<temp['nodes'].length; i++) 
		{
			//console.log('ingesting nodes: ' + temp['nodes'][i]['uid']);		

			var nodeinfoname = temp['nodes'][i]['type']['name'];
			
			// go through all existing nodetypes in sourcecolumn and select as mytype
			var mytype = null;
			for (var k = 0; k < this.s.sourcetypes.length; k++) 
			{
				for (var j=0; j < this.s.sourcetypes[k]['nodes'].length && mytype==null; j++) 
				{
					var thisname = this.s.sourcetypes[k]['nodes'][j].toString();

					if ( nodeinfoname === thisname) 
					{
						mytype = this.s.sourcetypes[k]['nodes'][j];
					}
				}
			}
			
			if (mytype != null) 
			{
				// instantialize this node
				var thisnode = this.add_node(
								temp['nodes'][i]['name'],
								deepCopy(mytype).init(),
								temp['nodes'][i]['x'],
								temp['nodes'][i]['y'],
								temp['nodes'][i]['radius']
							);

				// Create the association between the old and the new ID
				node_convert[temp['nodes'][i]['uid']] = thisnode.uid;

				//console.log(temp['nodes'][i]['uid'] +' -> '+ thisnode.uid)
				
				//			thisnode.uid = temp['nodes'][i]['uid'];
								
				if (thisnode.uid > highest_uid) highest_uid = thisnode.uid;
				
				// set the nodetype properties
				for( tY in temp['nodes'][i]['type'] ) 
				{
					thisnode['type'][tY] = temp['nodes'][i]['type'][tY];
				}
				
				// If a parent node was specified, the new tree is just  
				// of the main tree, so there extra stuff to do here
				if (parent)
				{			
					//Check if we're processing the root node from the new tree
					if (root_node)
					{
						// Let's make it a child of the specified parent node
						parent.add_child(thisnode);

						// Now we calculate the position offset, so that the new
						// tree will appear below the specified parent node
						x_offset = parent.x-thisnode.x;
						y_offset = parent.x-thisnode.x + 50;
						
						console.log('x_offset:'+x_offset+'  y_offset:'+y_offset);
						root_node = false;						
					}
					
					// Move the new nodes to the desired position below the parent
					thisnode.x = thisnode.x + x_offset;
					thisnode.y = thisnode.y + y_offset;

					// Lock all the new nodes
					thisnode.locked = true;					
				}
				
			} else 
			{
				alert(t[lang]['unknown_type']);
			}
			
		}
		
		// set generateuid on last known node id, redo make_uid_function class
		this.uid_function.set_last_uid(highest_uid);
		
		// import connections
		for (var i=0; i<temp['connections'].length; i++) 
		{
			var cname = temp['connections'][i];
			var res = cname.split(':');
			var node1 = null;
			var node2 = null;
			
			// Look for the nodes on this specific connection
			for (var j=0; j<this.nodes.length; j++) 
			{
				// node_convert is used to convert the old ids into the new ones
				if (node_convert[res[0]] == this.nodes[j].toString()) node1 = this.nodes[j];
				if (node_convert[res[1]] == this.nodes[j].toString()) node2 = this.nodes[j];
				// If both nodes were found, don't need to look any more
				if (node1 !== null && node2 !== null) break;
			}
			
			//console.log('outofloop: ' + node1.toString() + ' ' + node2.toString());
			if ((node1 != null) && (node2 != null)) 
			{			
				node1.add_child(node2);
			} 
			else
			{
				alert(t[lang]['connections_error']);
				//console.log(t[lang]['connections_error']);
			}
		}
		
		this.jsonstring = '';
	},

	export_to_json: function() 
	{
		var textToWrite = g.export_json();
		console.log(textToWrite);
		
		var textFileAsBlob = new Blob([textToWrite], {type:'text/plain'});
		var fileNameToSaveAs = 'export.json';

		var downloadLink = document.createElement("a");
		downloadLink.download = fileNameToSaveAs;
		downloadLink.innerHTML = "Download File";
		if (window.webkitURL != null)
		{
			// Chrome allows the link to be clicked
			// without actually adding it to the DOM.
			downloadLink.href = window.webkitURL.createObjectURL(textFileAsBlob);
		}
		else
		{
			// Firefox requires the link to be added to the DOM
			// before it can be clicked.
			downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
			downloadLink.onclick = function(event) {
					document.body.removeChild(event.target);
				};
			downloadLink.style.display = "none";
			document.body.appendChild(downloadLink);
		}

		downloadLink.click();
		
	},

	upload_to_arduino: function() 
	{
		console.log("upload_to_arduino");
		var textToWrite = g.export_arduino();
		
		
		sendData(textToWrite);
		
	},
	
	load_from_file: function()
	{
		//@todo: confirm we will load json (lose unsaved changes)
		var x;
		var r = window.confirm(t[lang]['confirm_load']);
		if (r==true)
		{
			//this.reset_graph();
		  	var fileToLoad = document.getElementById("fileToLoad").files[0];
			var fileReader = new FileReader();
			fileReader.onload = function(fileLoadedEvent) 
			{
				var textFromFileLoaded = fileLoadedEvent.target.result;
				g.import_json(textFromFileLoaded);
			};
			fileReader.readAsText(fileToLoad, "UTF-8");
			startAnimation();        
		  
		} else {
		  return;
		}
	},
	
	
};
