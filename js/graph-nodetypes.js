// Base node type identifiers (for arduino)
var NODE_SEQUENCE = 0x00;
var NODE_PARALLEL =	0x01;
var NODE_OVERRIDE = 0x02;
var NODE_SERVICE = 0x03;

// Service type codes (for arduino)
var SERVICE_DELAY = 0x00;
var SERVICE_OUTPUT_DIGITAL_PIN = 0x10;
var SERVICE_INPUT_DIGITAL_PIN = 0x20;

// Node state 
var NODE_STATE_FAILURE = 0;
var NODE_STATE_RUNNING = 1;
var NODE_STATE_SUCCESS = 2;
var NODE_STATE_INACTIVE = 3;	

//
// Video
//

function NodeTypeVideo()
{
	this.name = 'Video';
	this.init();
	return this.name;
}

NodeTypeVideo.prototype = 
{
	init: function() 
	{
		this.unique = false;
		this.leaf = true;
		return this;
	},
	toString: function() {
		return this.name;
	},
	set_color: function(color) {
		this.color = color;
	},
	list_properties: function() {
		return '';
	},
	add_list_properties_listeners: function() {
		return;
	},
	export_nodetype: function() {
		var output = {
			'name': this.name
		};
		return output;
	},
	export_arduino: function(nodeID) {
		return '////TODO';
	}
}



//
// Image
//

function NodeTypeImage() {
	this.name = 'Image';
	this.init();
	return this.name;
}

NodeTypeImage.prototype = {
	init: function() {
		this.unique = false;
		this.leaf = true;
		return this;
	},
	toString: function() {
		return this.name;
	},
	set_color: function(color) {
		this.color = color;
	},
	list_properties: function() {
		return '';
	},
	add_list_properties_listeners: function() {
		return;
	},
	export_nodetype: function() {
		var output = {
			'name': this.name
		};
		return output;
	},
	export_arduino: function(nodeID) {
		return '////TODO';
	}
}





