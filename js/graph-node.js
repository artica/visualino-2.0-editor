// Size proportion between the node and the collapse button
var COLLAPSE_BUTTON_SIZE = 10;


function Node(name, type) 
{
	this.init();
	this.name = name ? name : '';
	this.type = type ? type : '';
	return this.uid;
}

Node.prototype = 
{
	init: function() 
	{
		this.uid = g.uid_function.get_uid();
		this.parent = null;
		this.children = [];
		this.highlighted = false;
		this.connections_selected = [];
		this.selected = false;
		this.locked = false;
		this.expanded = true;
		this.x = -1;
		this.y = -1;
		this.display_x = -1;
		this.display_y = -1;
		this.radius = 10;
		this.strokeColor = "rgb(0,0,0)";
		this.lineWidth = 3;
		this.animation = 1.0;
		this.animation_direction = 1;
		this.state = NODE_STATE_INACTIVE;		
		this.display = false;
		return this;
	},
	// Sets the node parent
	add_parent: function (v) 
	{
		this.parent = v;
		return v;
	},
	// Removes the node parent
	remove_parent: function (v) 
	{
		this.parent = null;
		return v;
	},
	// Adds a new child to this node, making sure it's not repeated
	add_child: function (v) 
	{
		// Check if the child already exists on this list
		if (v in this.children) return null;
		this.children.push(v);
		
		// Adds this node as the parent of the parameter node
		v.add_parent(this);
		
		return v;
	},
	// Remove a child from this node
	remove_child: function (v) 
	{
		console.log('parent:'+this+'   remove_child:'+v);
		console.log(v);
		// If the desired child does not exist, nothing to do here...	
		var index = this.children.indexOf(v);
		console.log('index:'+this.children.indexOf(v));		
		if (index === -1) return null;

		// Removes this node as the parent of the parameter node
		v.remove_parent(this);
		
		this.children.splice(index, 1);
		console.log('REMOVED!');
		return v;
	},
	set_position: function (x, y)
	{
		//console.log('setting position to ' + x + ' ' + y);
		this.x = x;
		this.y = y;
		return this;
	},

	set_state: function(state)
	{
		/*
		console.log('set_state:' + this.id + '   '+state);
		console.log('state:' + state);
		console.log('NODE_STATE_INACTIVE:' + NODE_STATE_INACTIVE);
		console.log('NODE_STATE_RUNNING:' + NODE_STATE_RUNNING);


		console.log('state==NODE_STATE_INACTIVE :' + (state==NODE_STATE_INACTIVE) );
		console.log('state==NODE_STATE_RUNNING :' + (state==NODE_STATE_RUNNING) );

		if (state == NODE_STATE_INACTIVE) console.log('setting node:' + this.id + '   NODE_STATE_INACTIVE');
		if (state == NODE_STATE_RUNNING) console.log('setting node:' + this.id + '   NODE_STATE_RUNNING');
		*/

		this.state = state;
/*		
		// When a node is set as inactive or starts to run, all children nodes become inactives
		if ( (state == NODE_STATE_INACTIVE || state == NODE_STATE_RUNNING) && this.children.length>0 )
		{
			for (var i=0; i<this.children.length; i++)
			{
				this.children[i].set_state(NODE_STATE_INACTIVE);
			}
		}
*/		
	},
	// Checks all the ancestors of a specific node up to the root
	// of the tree, looking for a specific node
	check_ancestors: function (n)
	{
		console.log('check_ancestors:' + n);
		// Reached the top of the tree, the node wasn't found
		if (this.parent === null) return false;
		else
		{
			// The node was found, so we return true
			if (this.parent === n) return true;
			
			// At this point the node wasn't found, so we go up on the tree
			return this.parent.check_ancestors(n);
		}
	},
	move_node: function (x, y)
	{
		//console.log('moving node from ' + this.x + ' ' + this.y + ' to ' + (this.x -x) + ' ' + (this.y -y) );
		this.x -= x;
		this.y -= y;
		return this;
	},

	set_radius: function (radius) 
	{
		this.radius = radius;
		return this;
	},
	
	reset_animation: function (value) 
	{
		this.animation = (value === 0? 1.0: 0.0);
		this.animation_direction = value;
		this.display = (value === 0);
		
		//console.log('reset_animation   direction:'+this.animation_direction+
		//	'  animation:'+this.animation+'  display:'+this.display);
		return this;
	},
	
	// Marks the node as collapsed and collapse it
	collapse: function () 
	{
		this.expanded = false;
		this.collapse_children();
	},
	
	// Collapse this node and all its children
	collapse_node: function () 
	{
		this.animation_direction = 0;
		this.collapse_children();
		return this;
	},
	
	// Collapse all the children of this node
	collapse_children: function () 
	{
		// Let's check each node for its children
		for (var j=0; j<this.children.length; j++)
		{
			var child = this.children[j];
			if (!child) return;
			child.collapse_node();
		}	
	},
	
	// Marks the node as expanded and expands it
	expand: function () 
	{
		this.expanded = true;
		this.expand_children();
	},
	
	// Expand this node and all its children
	expand_node: function () 
	{
		this.animation_direction = 1;
		// Only expands the children if the node is expanded
		if (this.expanded) this.expand_children();
		return this;
	},
	
	// Expand all the children of this node
	expand_children: function () 
	{
		// Let's check each node for its children
		for (var j=0; j<this.children.length; j++)
		{
			var child = this.children[j];
			if (!child) return;
			child.expand_node();
		}	
	},
	
	update_animation: function () 
	{
		// Animation to show the nodes
		if (this.animation_direction === 1)
		{
			// We don't need to update if the parent node is still animating
			if ( (this.parent != null && this.parent.animation<1.0 ) ) return true; 
			
			// Advance the animation to the next frame
			// The node starts moving faster and gets slower as it gets
			// closer to the final position
			var step = Math.max(0.05, (1.0-this.animation)*0.2);
			this.animation += step;
			if (this.animation > 1.0) this.animation = 1.0;
			this.display = true;
		}
		else 		
		// Animation to hide the nodes
		{		
			// We don't need to update if this is a root node, or if 
			// any of the children nodes are still animating
			if (this.parent === null) return true; 
			for (var j=0; j<this.children.length; j++)
			{
				var child = this.children[j];
				if (!child || child.animation>0.1) return true;
			}
			// Advance the animation to the next frame
			// The node starts moving slow and gets faster as it gets
			// closer to the parent node
			var step = Math.max(0.05, (1.0-this.animation)*0.2);
			this.animation -= step;
			if (this.animation <= 0.0)
			{
				this.animation = 0.0;
				this.display = false;
			}
			//console.log('step:'+step);
			//console.log('anim:'+this.animation);
		}
		
		//console.log('this.animation:'+this.animation);
		// Change the display position, depending on if the node has a parent or not
		this.display_x = this.x;
		this.display_y = this.y;
		if (this.parent != null)
		{
			this.display_x = this.parent.x + ( this.x - this.parent.x ) * this.animation;
			this.display_y = this.parent.y + ( this.y - this.parent.y ) * this.animation;
		}
		
		return ( (this.animation > 0.0 && this.animation_direction === 0 ) ||
				  this.animation < 1.0 && this.animation_direction === 1 );
	},	
	
	draw: function (ctx) 
	{
		// Check is this node is visible or not
		if (this.display === false) return; 

		ctx.lineWidth = this.lineWidth;

		// Default colors
		var stroke_style = "transparent";
		var fill_style = this.type.color;

		// Change the aspect depending on if the node is locked
		if (this.locked) stroke_style = '#3366ff';


		switch (this.state)
		{
			case NODE_STATE_FAILURE: stroke_style = '#ff0000'; break;
			case NODE_STATE_RUNNING: stroke_style = '#0000ff'; break;
			case NODE_STATE_SUCCESS: stroke_style = '#00ff00'; break;
			case NODE_STATE_INACTIVE: stroke_style = '#999999'; break;
		}

		// Change the aspect depending on if the node is selected
		if (this.highlighted || this.selected) stroke_style = this.strokeColor;
		
		drawCircle( ctx, this.display_x,this.display_y, this.radius * g.screen_zoom, fill_style, stroke_style );

		drawText ( ctx, this.name, g.corner_x+this.display_x-13, g.corner_y+this.display_y-8-14, "#ffffff" );
		drawText ( ctx, t[lang][this.type.toString()], g.corner_x+this.display_x-13, g.corner_y+this.display_y-8, "#ffffff" );

		// If the node is not a leaf, draw the button to expand/collapse the tree
		if (this.children.length > 0)
		{
			drawCircle( ctx, g.corner_x+this.display_x+this.radius+COLLAPSE_BUTTON_SIZE,
				g.corner_y+this.display_y, COLLAPSE_BUTTON_SIZE, fill_style, stroke_style );
			ctx.beginPath();
			ctx.lineCap="round";
			ctx.lineWidth = 3;
			ctx.strokeStyle = '#000';
			
			ctx.moveTo(g.corner_x+this.display_x+this.radius+COLLAPSE_BUTTON_SIZE/2, g.corner_y+this.display_y);
			ctx.lineTo(g.corner_x+this.display_x+this.radius+COLLAPSE_BUTTON_SIZE*1.5, g.corner_y+this.display_y);
			if (this.expanded === false)
			{
				ctx.moveTo(	g.corner_x+this.display_x+this.radius+COLLAPSE_BUTTON_SIZE, 
							g.corner_y+this.display_y-COLLAPSE_BUTTON_SIZE/2);
				ctx.lineTo(g.corner_x+this.display_x+this.radius+COLLAPSE_BUTTON_SIZE, 
							g.corner_y+this.display_y+COLLAPSE_BUTTON_SIZE/2);
			}
			ctx.stroke();			
		}
		
		if (typeof(this.type.draw) === "function") 
		{ 
   			this.type.draw(ctx, g.corner_x+this.display_x, g.corner_y+this.display_y, this.radius);			
		}
		
		return this;
	},
	draw_connections: function (ctx) 
	{
		// Check if this node is visible or not
		if (!this.display) return; 
		
		// Let's check each node for its children
		for (var j=0; j<this.children.length; j++)
		{
			var child = this.children[j];
			if (!child) return;

			// Check if the child node is visible or not
			if (!child.display) continue; 
			var color = 'rgb(200,200,200)';
			
			// Check if the connection is on the selected list
			if (this.connections_selected.indexOf(child) > -1)
			//if (child in this.connections_selected)
			{
				color = 'rgb(255,50,25)';
				//console.log('SELECTED!!!')
			}
			ctx.lineWidth = this.lineWidth
			ctx.strokeStyle = color;
			ctx.beginPath();
			ctx.moveTo(g.corner_x+this.display_x, g.corner_y+this.display_y);
			ctx.lineTo(g.corner_x+child.display_x, g.corner_y+child.display_y);
			ctx.stroke();
		}
	},

	highlight: function () 
	{
		this.highlighted = true;
		return this;
	},
	unhighlight: function () 
	{
		this.highlighted = false;
		return this;
	},
	connection_select: function (c) 
	{
		console.log('connection_select:'+c)
		// Check if the child already exists on this list
		if (this.connections_selected.indexOf(c) > -1) return null;
		
		this.connections_selected.push(c);
		return this;
	},
	connection_unselect: function (c) 
	{
		this.connections_selected = [];
		return this;
	},
	// Removes all selected connections
	connection_remove: function () 
	{
		if (this.connections_selected.length===0) return this;
		
		console.log('connection_remove:'+this.connections_selected.length)
		for (var i=0; i<this.connections_selected.length; i++)
		{
			this.remove_child(this.connections_selected[i]);
		}
		this.connections_selected = [];
		return this;
	},
	
	toggle_highlight: function () 
	{
		if (this.highlighted) {
			return this.unhighlight();
		} else {
			return this.highlight();
		}
	},
	select: function () 
	{
		this.selected = true;
		return this;
	},
	select_tree: function () 
	{
		this.selected = true;
		for (var i=0; i<this.children.length; i++)
		{
			this.children[i].select_tree();
		}
		return this;
	},
	unselect: function () 
	{
		this.selected = false;
		return this;
	},
	toggle_selection: function () 
	{
		if (this.selected) 
		{
			return this.unselect();
		} 
		else 
		{
			return this.select();
		}
	},
	toString: function() 
	{
		return this.uid;
	},
	
	
	//
	// export / import
	//
	
	export_node: function() 
	{
		var output = 
		{
			'name': this.name,
			'type': this.type.export_nodetype(),
			'uid': this.uid,
			'x': this.x,
			'y': this.y,
			'radius': this.radius
		};
		return output;
	},
	
	export_connection: function(c) 
	{
		//c1 = c1.toString();
		//c2 = c2.toString();

		//console.log('export_connection:'+c+'   '+this.uid+'->'+this.children[c].uid)
		console.log('export_connection:'+c+'   '+this.uid+'->');
		console.log(this);
		console.log(this.children);
		console.log(this.children[c]);
		
		return this.uid + ':' +this.children[c].uid ;
	},	
	
	// Sort the children of this node recursively, creating 
	// new sequential ID's for the arduino 
	sort_children: function (new_id) 
	{
		// Set the new arduino node id (sequencial order)
		this.id = new_id;
		
		console.log('this is:'+new_id);
		// Sort this node's children by the X coordinate
		this.children.sort(function(node1, node2) 
			{
				return node1.x - node2.x;
			});
			
		var current_id = new_id+1;
		// Sort all the children nodes recursively
		for (var j=0; j<this.children.length; j++)
		{	
			// The of each children is the highest id of the previous
			// children branch plus 1
			current_id = this.children[j].sort_children(current_id) + 1;
		}
		return current_id-1;
	},
	
	export_arduino: function() 
	{
		return this.type.export_arduino( this );
	},
	
	// Create a string with all the children connections
	export_connections: function () 
	{
		var connections = '';
		for (var j=0; j<this.children.length; j++)
		{	
			connections += 'C'+padByte(this.id)+
				padByte(this.children[j].id) + ';';
		}
		return connections;
	},
	
	
}
