var socket = new WebSocket("ws://127.1.1.1:3000/");        


socket.onopen = function() 
{
	console.log("BAM!");
	//do something when connection estabilished
};

socket.onmessage = function(message) 
{
	//console.log(message.data);
	var tokens = message.data.split(',');

	switch(tokens[0])
	{
		case 'NS':
			for (var i=1; i<tokens.length-1; i++)
			{
				var node_tokens = tokens[i].split(':');
				var node = parseInt(node_tokens[0], 16);
				var state = parseInt(node_tokens[1], 16);
				g.set_node_state(node, state);
			}
			g.draw(ctx);
		break;
		case 'L':
			//console.log("loop");
			g.draw(ctx);
		break;
	}
};

socket.onclose = function()
{
	console.log("Websocket closed!");
};

var send_buffer = [];

function sendNextBlock()
{
	if (send_buffer.length > 0)
	{
		socket.send(send_buffer[0]+';');
		send_buffer.splice(0, 1);
		setTimeout(sendNextBlock, 25);	
	}
}

function sendData(text)
{
	console.log('sendData:'+text);
	var tokens = text.split(';'); 
	for (var i=0; i<tokens.length-1; i++)
	{
		send_buffer.push(tokens[i]);
		
	}
	sendNextBlock();
}