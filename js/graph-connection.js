
function Connection( c1, c2, temp ) {
	this.c1 = c1;
	this.c2 = c2;
	this.lineWidth = 5;
	this.temp = temp ? temp : false;
	this.highlighted = false;
}

Connection.prototype = {
	generate_name: function (c1, c2) {
		c1 = c1.toString();
		c2 = c2.toString();

		// Ensure that c1 <= c2
		if (c2 < c1) {
			var tmp = c2;
			c2 = c1;
			c1 = tmp;
		}

		return c1 + ':' + c2;
	},
	toString: function () {
		return this.generate_name(this.c1, this.c2);
	},
	highlight: function () {
		this.highlighted = true;
		return this;
	},
	unhighlight: function () {
		this.highlighted = false;
		return this;
	},
	draw: function (ctx) {
		var color = 'rgb(200,200,200)';
		if (this.highlighted) {
			color = 'rgb(255,255,255)';
		}
		ctx.lineWidth = this.lineWidth;
		ctx.strokeStyle = color;
    	ctx.beginPath();
    	ctx.moveTo(this.c1.x, this.c1.y);
    	ctx.lineTo(this.c2.x, this.c2.y);
    	ctx.stroke();
	}
}
