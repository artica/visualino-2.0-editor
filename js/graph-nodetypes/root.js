
//
// Root
//

function NodeTypeRoot() {
	this.name = 'Root';
	this.init();
	return this.name;
}

NodeTypeRoot.prototype = {
	init: function() {
		this.unique = true;
		this.leaf = false;
		this.targets = [
						{	'name': 'Farrusco-1',
							'model': 'Arduino Uno',
							'assigns':[
								{
									// all these names and properties have translation
									'Name': 'Servo',
									'Node Type':'Servo',
									'Pin': 9
								},
								{
									'Name': 'Left Motor',
									'Node Type': 'Motor',
									'Pin 1': 3,
									'Pin 2': 5
								},
								{
									'Name': 'Right Motor',
									'Node Type': 'Motor',
									'Pin 1': 6,
									'Pin 2': 11
								},
								{
									'Name': 'Sensor IR',
									'Node Type': 'Sensor',
									'Pin Type': 'Analogue',
									'Pin': 0
								},
								{
									'Name': 'Left Bumper',
									'Node Type': 'Sensor',
									'Pin Type': 'Digital',
									'Pin': 7
								},
								{
									'Name': 'Right Bumper',
									'Node Type': 'Sensor',
									'Pin Type': 'Digital',
									'Pin': 8
								}
								
							]
						},
						{	'name':'Farrusco-2',
							'assigns': []
						}
					   ];
		this.selectedtarget = 0;
		
		return this;
	},
	toString: function() {
		return this.name;
	},
	set_color: function(color) {
		this.color = color;
	},
	list_properties: function() {
		var output = '';
		output += '<tr><td>'+t[lang]['Target']+'</td><td>';
		output += '<select id="targetInput">';
		for (var i=0; i<this.targets.length; i++) {
			var selected = '';
			if (this.selectedtarget == i) selected = 'selected="true"';
			output += '<option value="'+i+'" '+selected+'>' + this.targets[i]['name'] + '</option>';
		}
		output += '</td></tr>';
		
		var assigns = this.targets[this.selectedtarget]['assigns'];
		for (var i=0; i<assigns.length; i++) {
			output += '<tr><td><hr></td><td><hr></td></tr>';
			for (j in assigns[i]) {
				var prop = assigns[i][j];
				switch(j) {
					case 'Name':
						output += '<tr><td><b>'+t[lang][assigns[i]['Node Type']]+'</b></td><td><b>'+t[lang][prop]+'</b></td></tr>';
					break;
					case 'Node Type':
					break;
					default:
				output += '<tr><td>'+t[lang][j]+'</td><td>'+prop+'</td></tr>';
					break;
				}
			}
		}

		return output;
	},
	add_list_properties_listeners: function() {
	
		var targetInput = document.getElementById('targetInput');
		if (targetInput) {
			var thisnode = this;
			targetInput.onchange = function() {
				//console.log('changed something');
				var index = targetInput.selectedIndex;
				var options = targetInput.options;
				thisnode.selectedtarget = options[index].value;
				
				if (g.selectedNode != -1) g.display_node_properties(g.selectedNode);
				
				g.target_platform_changed();
			}
		}
	
		return;
	},
	export_nodetype: function() {
		var output = {
			'name': this.name
		};
		return output;
	},
	export_arduino: function(node) 
	{
		return 'N' + padByte(node.id) + padByte(NODE_SEQUENCE) + 
			padByte(node.children.length)+';';
	}
}
