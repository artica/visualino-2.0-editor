
//
// Priority Selector
//

function NodeTypePrioritySelector() {
	this.name = 'Priority Selector';
	this.init();
	return this.name;
}

NodeTypePrioritySelector.prototype = {
	init: function() {
		this.unique = false;
		this.leaf = false;
		return this;
	},
	toString: function() {
		return this.name;
	},
	set_color: function(color) {
		this.color = color;
	},
	list_properties: function() {
		return '';
	},
	add_list_properties_listeners: function() {
		return;
	},
	export_nodetype: function() {
		var output = {
			'name': this.name
		};
		return output;
	},
	export_arduino: function(nodeID) {
		return 'BehaviorTree::PriorityNode* ' + nodeID + ' = new BehaviorTree::PriorityNode();';
	}
}
