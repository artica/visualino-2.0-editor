
//
// Json Import
// 

function NodeTypeJsonImport() 
{
	this.name = 'JSON Import';
	this.init();

	return this.name;
}

NodeTypeJsonImport.prototype = 
{
	init: function() {
		this.unique = false;
		this.leaf = false;
		this.jsonstring = '';
		this.detailshidden = true;
		
		//todo: parse json and load as properties any binded parameters
		
		return this;
	},
	toString: function() {
		return this.name;
	},
	set_color: function(color) {
		this.color = color;
	},
	list_properties: function() {
		
		var output = '';
		
		//todo: if hidden, place button to show detail
		if (this.detailshidden) {
			//todo: place button to show detail
		}
		
		// JSON File To Load
		output += '<tr><td>'+t[lang]['JSON String']+'</td><td><input id="jsonstringFileToLoad" type="file" /></td></tr>';
	
		// JSON String
		//output += '<tr><td>'+t[lang]['JSON String']+'</td><td><textarea id="jsonstringInput" name="jsonstringInput">' + this.jsonstring + '</textarea></td></tr>';
		
		//todo: list jsonfile or jsonstring
		//todo: list any binded parameters
		return output;
	},
	add_list_properties_listeners: function() 
	{
	
		var thisnode = this;
		
		// JSON String Input Box
		/*var jsonstringInputBox = document.getElementById('jsonstringInput');
		if (jsonstringInputBox) {
			jsonstringInputBox.onkeyup = function() {
				g.inputBoxTyping = true;
				thisnode.jsonstring = jsonstringInputBox.value;
			}
			jsonstringInputBox.onclick = function() {
				g.inputBoxTyping = true;
			}
			jsonstringInputBox.onfocus = function() {
				g.inputBoxTyping = true;
			}
		}*/
		
		// JSON File To Load
		var jsonstringFileToLoad = document.getElementById('jsonstringFileToLoad');
		jsonstringFileToLoad.onchange = function() 
		{
			console.log('load json file...');
			console.log(thisnode);
			
			var fileReader = new FileReader();
			fileReader.onload = function(fileLoadedEvent)
			{
				console.log(thisnode);
				var textFromFileLoaded = fileLoadedEvent.target.result;
//				thisnode.jsonstring = textFromFileLoaded;
				
				
				
				// @todo isto nao pode sacar o no assim...
				console.log('g.lastSelectedNode:'+g.lastSelectedNode)
				g.import_json(textFromFileLoaded, g.nodes[g.lastSelectedNode]);
				//g.import_json(textFromFileLoaded, thisnode);
				
//				thisnode.consumeJSONString();
			};
			fileReader.readAsText(jsonstringFileToLoad.files[0], "UTF-8");
			
		}
		
		//todo: update jsonfile or jsonstring
		//todo: update any binded parameters
		return;
	},
	/*
	consumeJSONString: function() {
	
		var diffx = 0;
		var diffy = 0;
		var thisnoderef = null;
		
		// don't progress if node has children
		for (var i = 0; i < g.nodes.length; i++) {	
			if ( (g.nodes[i].selected) && (g.nodes[i].type.name == 'JSON Import')) {
				
				// save x and y diff, will be needed below
				diffx = g.nodes[i]['x'];
				diffy = g.nodes[i]['y'];
				thisnoderef = g.nodes[i];
				
				// if it has children, don't proceed
				if (g.nodes[i].list_neighbours().length > 1) {
					alert(t[lang]['JSON Child']);
					return;
				}
			}
		}
	
		// don't progress without a string
		if (this.jsonstring == '') return;
		
		// parse json
		var temp = JSON.parse(this.jsonstring);

		// find root, get it's x and y diff	
		for (var i=0; i<temp['nodes'].length; i++) {
			if (temp['nodes'][i]['type']['name'] == 'Root') {
				diffx -= temp['nodes'][i]['x'];
				diffy -= temp['nodes'][i]['y'];
			}

		}
		
		// import nodes
		for (var i=0; i<temp['nodes'].length; i++) {			

			var nodeinfoname = temp['nodes'][i]['type']['name'];
			
			// go through all existing nodetypes in sourcecolumn and select as mytype
			var mytype = null;
			for (var k = 0; k < g.s.sourcetypes.length; k++) {
				for (var j=0; j < g.s.sourcetypes[k]['nodes'].length && mytype==null; j++) {
					var thisname = g.s.sourcetypes[k]['nodes'][j].toString();

					if ( nodeinfoname === thisname) {
						mytype = g.s.sourcetypes[k]['nodes'][j];
					}
				}
			}
			
			if (mytype != null) {
			
				// don't add root, we will replace it for json import on connections
				
				if (nodeinfoname == 'Root') {
					
					temp['nodes'][i]['newuid'] = thisnoderef['uid'];
					
				} else {
			
					// instantialize this node
					var thisnode = g.add_node(
									temp['nodes'][i]['name'],
									deepCopy(mytype).init(),
									temp['nodes'][i]['x']+diffx,
									temp['nodes'][i]['y']+diffy,
									temp['nodes'][i]['radius']
								);
							
					//store reference of new uid to replace on connections
					temp['nodes'][i]['newuid'] = thisnode.uid;
				
					// set the nodetype properties
					for( tY in temp['nodes'][i]['type'] ) {
						thisnode['type'][tY] = temp['nodes'][i]['type'][tY];
					}
					
				}
				
			} else {
				alert(t[lang]['unknown_type']);
			}
					 
		}

		// import connections
		for (var i=0; i<temp['connections'].length; i++) {		
			var cname = temp['connections'][i];
			var res = cname.split(':');
			var node1 = null;
			var node2 = null;
						
			// patch reference to res0
			for (var oldi=0; oldi<temp['nodes'].length; oldi++) {
				console.log('cacaca ' + oldi + ' ' + temp['nodes'][oldi]['uid'] + ' ' + temp['nodes'][oldi]['newuid']);
				if (res[0] == temp['nodes'][oldi]['uid']) {
					res[0] = temp['nodes'][oldi]['newuid'];
					break;
				}
			}
			
			// patch reference to res1
			for (var oldi=0; oldi<temp['nodes'].length; oldi++) {
				if (res[1] == temp['nodes'][oldi]['uid']) {
					res[1] = temp['nodes'][oldi]['newuid'];
					break;
				}
			}

			for (var j=0; j<g.nodes[j]; j++) {
				if (res[0] == g.nodes[j].toString()) node1 = g.nodes[j];
				if (res[1] == g.nodes[j].toString()) node2 = g.nodes[j];
			}
			
			if ((node1 != null) && (node2 != null)) {
				g.add_connection(node1, node2);
			} else {
				alert(t[lang]['connections_error']);
			}
		}
		
	},
	*/
	export_nodetype: function() {
		var output = {
			'name': this.name,
			'jsonstring': this.jsonstring
		};
		return output;
	},
	export_arduino: function(nodeID) {	
		//export as a normal sequence node
		return 'JSON* ' + nodeID + ';';
	}
}
