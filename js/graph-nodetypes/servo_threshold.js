
//
// Servo Threshold
//

function NodeTypeServoThreshold() {
	this.name = 'Servo Threshold';
	this.init();
	return this.name;
}

NodeTypeServoThreshold.prototype = {
	init: function() {
		this.unique = false;
		this.leaf = true;
		//this.pintypes = ['Analogue','Digital','PWM'];
		//this.pintype = 'Analogue';
		//this.pinvalue = 9;
		this.threshold = 90;
		this.lowhigh = 0;
		return this;
	},
	toString: function() {
		return this.name;
	},
	set_color: function(color) {
		this.color = color;
	},
	list_properties: function() {
		var output = '';

		// threshold
		output += '<tr><td>'+t[lang]['Threshold']+'</td><td><input id="thresholdInput" name="thresholdInput" value="'+this.threshold+'"></td></tr>';
		
		// lowhigh
		output += '<tr><td>'+t[lang]['Pass Type']+'</td><td>';
		output += '<select id="lowhighInput">';
		output += '<option value="0"';
		if (this.lowhigh == 0) output += ' selected="true"';
		output += '>' + t[lang]['Pass Low'] + '</option>';
		output += '<option value="1"';
		if (this.lowhigh == 1) output += ' selected="true"';
		output += '>' + t[lang]['Pass High'] + '</option>';
		output += '</select></td></tr>';
		
		return output;
	},
	add_list_properties_listeners: function() {
	
		// pin type
		var pintypeInput = document.getElementById('pintypeInput');
		if (pintypeInput) {
			var thisnode = this;
			pintypeInput.onchange = function() {
				var index = pintypeInput.selectedIndex;
				var options = pintypeInput.options;
		
				for (var j = 0; j < thisnode.pintypes.length; j++) {
					if (options[index].value == thisnode.pintypes[j]) {
						thisnode.pintype = thisnode.pintypes[j];
					}
				}
			}
		}
		
		// threshold
		var thresholdInputBox = document.getElementById('thresholdInput');
		if (thresholdInputBox) {
			var thisnode = this;
			thresholdInputBox.onkeyup = function() {
				g.inputBoxTyping = true;
				thisnode.value = thresholdInputBox.value;
			}
			thresholdInputBox.onclick = function() {
				g.inputBoxTyping = true;
			}
			thresholdInputBox.onfocus = function() {
				g.inputBoxTyping = true;
			}
		}
		
		// lowhigh
		var lowhighInputBox = document.getElementById('lowhighInput');
		if (lowhighInputBox) {
			var thisnode = this;
			lowhighInputBox.onkeyup = function() {
				g.inputBoxTyping = true;
				thisnode.lowhigh = parseInt(lowhighInputBox.value, 10);
			}
			lowhighInputBox.onclick = function() {
				g.inputBoxTyping = true;
			}
			lowhighInputBox.onfocus = function() {
				g.inputBoxTyping = true;
			}
		}
	
		return;
	},
	export_nodetype: function() {
		var output = {
			'name': this.name,
			'threshold': this.threshold,
			'lowhigh': this.lowhigh
		};
		return output;
	},
	export_arduino: function(nodeID) {
		return 'SL::Behaviors::ServoThreshold* ' + nodeID + ' = new SL::Behaviors::ServoThreshold('+this.threshold+','+this.lowhigh+');';
	}
}

