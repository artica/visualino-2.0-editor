
//
// LED
//

function NodeTypeLED() {
	this.name = 'LED';
	this.init();
	return this.name;
}

NodeTypeLED.prototype = {
	init: function() {
		this.unique = false;
		this.leaf = true;
		this.pintypes = ['Analogue','Digital','PWM'];
		// analogue (0-5), digital (0-19), pwm (3,5,6,9,10,11)
		this.pintype = 'Digital';
		this.pinvalue = 13;
		this.value = 255;
		return this;
	},
	toString: function() {
		return this.name;
	},
	set_color: function(color) {
		this.color = color;
	},
	list_properties: function() {
		var output = '';
		
		// pin type		
		output += '<tr><td>'+t[lang]['Pin Type']+'</td><td>';
		output += '<select id="pintypeInput">';
		for (var i=0; i < this.pintypes.length; i++) {
			var selected = '';
			if (this.pintype == this.pintypes[i]) selected = 'selected="true"';
			output += '<option value="'+this.pintypes[i]+'" '+selected+'>' + this.pintypes[i] + '</option>';
		}
		output += '</td></tr>';

		// pin value
		output += '<tr><td>'+t[lang]['Pin']+'</td><td><input id="pinInput" name="pinInput" value="'+this.pinvalue+'"></td></tr>';
		
		// value
		output += '<tr><td>'+t[lang]['Value']+' [0..255]</td><td><input id="valueInput" name="valueInput" value="'+this.value+'"></td></tr>';
		
		return output;
	},
	add_list_properties_listeners: function() {
		
		// pin type
		var pintypeInput = document.getElementById('pintypeInput');
		if (pintypeInput) {
			var thisnode = this;
			pintypeInput.onchange = function() {
				var index = pintypeInput.selectedIndex;
				var options = pintypeInput.options;
		
				for (var j = 0; j < thisnode.pintypes.length; j++) {
					if (options[index].value == thisnode.pintypes[j]) {
						thisnode.pintype = thisnode.pintypes[j];
					}
				}
			}
		}
		
		// pin value
		var pinInputBox = document.getElementById('pinInput');
		if (pinInputBox) {
			var thisnode = this;
			pinInputBox.onkeyup = function() {
				g.inputBoxTyping = true;
				thisnode.pinvalue = pinInputBox.value;
			}
			pinInputBox.onclick = function() {
				g.inputBoxTyping = true;
			}
			pinInputBox.onfocus = function() {
				g.inputBoxTyping = true;
			}
		}
		
		// value
		var valueInputBox = document.getElementById('valueInput');
		if (valueInputBox) {
			var thisnode = this;
			valueInputBox.onkeyup = function() {
				g.inputBoxTyping = true;
				thisnode.value = valueInputBox.value;
			}
			valueInputBox.onclick = function() {
				g.inputBoxTyping = true;
			}
			valueInputBox.onfocus = function() {
				g.inputBoxTyping = true;
			}
		}
	
		return;
	},
	export_nodetype: function() {
		var output = {
			'name': this.name,
			'pintype': this.pintype,
			'pinvalue': this.pinvalue,
			'value': this.value
		};
		return output;
	},
	export_arduino: function(node) 
	{
		return 'N' + padByte(node.id) + 
			padByte(NODE_SERVICE) + padByte(SERVICE_OUTPUT_DIGITAL_PIN)+
			padByte(this.pinvalue)+padByte(this.value)+';';
	}
}
