
//
// Motor Control
//

function NodeTypeMotor() {
	this.name = 'Motor';
	this.init();
	return this.name;
}

NodeTypeMotor.prototype = {
	init: function() {
	
		this.targeted = 0;		
		this.assigned = -1;
		
		this.unique = false;
		this.leaf = true;
	
		//this.pintypes = ['Digital','PWM'];
		// digital (0-19), pwm (3,5,6,9,10,11)
		//this.pintype = 'Digital';
		this.pin1value = 5;
		this.pin2value = 6; 
		
		this.velocity = 0; // (0-255)
		return this;
	},
	toString: function() {
		var output = this.name;
		if (this.assigned != -1) {
			var nt = g.nodes[g.find_root()].type;
			output = nt.targets[nt.selectedtarget]['assigns'][this.assigned]['Name'];
		}
		return output;
	},
	set_color: function(color) {
		this.color = color;
	},
	list_properties: function() {
		var output = '';

		// assigned motor		
		output += '<tr><td>'+t[lang]['Assigned']+'</td><td>';
		output += '<select id="assignedInput">';
		
		var selected = '';
		if (this.assigned == -1) selected = 'selected="true"';
		output += '<option value="-1" '+selected+'></option>';
		
		var root_ref = g.find_root();
		
		if (root_ref != -1) {
			var nt = g.nodes[root_ref].type;
			var st = nt.selectedtarget;
			var assigns = nt.targets[st]['assigns'];
			for (var i=0; i < assigns.length; i++) {
				if (assigns[i]['Node Type'] == this.name) {
					var selected = '';
					if (i == this.assigned) selected = 'selected="true"';
					output += '<option value="'+i+'" '+selected+'>' + t[lang][assigns[i]['Name']] + '</option>';
				}
			}
		}
		output += '</select></td></tr>';
		
		
		//todo: if assigned to something, then grey out (or hide) the rest of the boxes

		// pin type		
		/*output += '<tr><td>'+t[lang]['Pin Type']+'</td><td>';
		output += '<select id="pintypeInput">';
		for (var i=0; i < this.pintypes.length; i++) {
			var selected = '';
			if (this.pintype == this.pintypes[i]) selected = 'selected="true"';
			output += '<option value="'+this.pintypes[i]+'" '+selected+'>' + t[lang][this.pintypes[i]] + '</option>';
		}
		output += '</select></td></tr>';
		*/
		// pin1 value
		output += '<tr><td>'+t[lang]['Pin 1']+'</td><td><input id="pin1Input" name="pin1Input" value="'+this.pin1value+'"></td></tr>';
		
		// pin2 value
		output += '<tr><td>'+t[lang]['Pin 2']+'</td><td><input id="pin2Input" name="pin2Input" value="'+this.pin2value+'"></td></tr>';
		
		// velocity
		output += '<tr><td>'+t[lang]['Velocity']+' [-255..255]</td><td><input id="velocityInput" name="velocityInput" value="'+this.velocity+'"></td></tr>';
		
		return output;
	},
	add_list_properties_listeners: function() {
		
		// assigned
		var assignedInput = document.getElementById('assignedInput');
		if (assignedInput) {
			var thisnode = this;
			assignedInput.onchange = function() {
				console.log('changing');
				var index = assignedInput.selectedIndex;
				var options = assignedInput.options;
				
				// update assigned index
				thisnode.assigned = parseInt(options[index].value,10);
				
				// update pin values
				if (thisnode.assigned != -1) {
					var nt = g.nodes[g.find_root()].type;
					var assigns = nt.targets[nt.selectedtarget]['assigns'][thisnode.assigned];
					//console.log('assigns');
					//console.log(assigns);
					//console.log('from ' + thisnode.pin1value + ' to ' + assigns['Pin 1']);
					
					thisnode.pin1value = assigns['Pin 1'];
					thisnode.pin2value = assigns['Pin 2'];
					
					if (g.selectedNode != -1) g.display_node_properties(g.selectedNode);
				}
			}
		}
		
		
		// pin type
		/*var pintypeInput = document.getElementById('pintypeInput');
		if (pintypeInput) {
			var thisnode = this;
			pintypeInput.onchange = function() {
				var index = pintypeInput.selectedIndex;
				var options = pintypeInput.options;
		
				for (var j = 0; j < thisnode.pintypes.length; j++) {
					if (options[index].value == thisnode.pintypes[j]) {
						thisnode.pintype = thisnode.pintypes[j];
					}
				}
			}
		}
		*/
		
		// pin1 value
		var pin1InputBox = document.getElementById('pin1Input');
		if (pin1InputBox) {
			var thisnode = this;
			pin1InputBox.onkeyup = function() {
				g.inputBoxTyping = true;
				thisnode.pin1value = pin1InputBox.value;
			}
			pin1InputBox.onclick = function() {
				g.inputBoxTyping = true;
			}
			pin1InputBox.onfocus = function() {
				g.inputBoxTyping = true;
			}
		}
		
		// pin2 value
		var pin2InputBox = document.getElementById('pin2Input');
		if (pin2InputBox) {
			var thisnode = this;
			pin2InputBox.onkeyup = function() {
				g.inputBoxTyping = true;
				thisnode.pin2value = pin2InputBox.value;
			}
			pin2InputBox.onclick = function() {
				g.inputBoxTyping = true;
			}
			pin2InputBox.onfocus = function() {
				g.inputBoxTyping = true;
			}
		}
				
		// velocity
		var velocityInputBox = document.getElementById('velocityInput');
		if (velocityInputBox) {
			var thisnode = this;
			velocityInputBox.onkeyup = function() {
				//if velocity is negative, switch value on pins
				g.inputBoxTyping = true;
				thisnode.velocity = velocityInputBox.value;
			}
			velocityInputBox.onclick = function() {
				g.inputBoxTyping = true;
			}
			velocityInputBox.onfocus = function() {
				g.inputBoxTyping = true;
			}
		}
		
		return;
	},
	export_nodetype: function() {
		var output = {
			'name': this.name,
			'pintype': this.pintype,
			'pin1value': this.pin1value,
			'pin2value': this.pin2value,
			'velocity': this.velocity
		};
		return output;
	},
	export_arduino: function(nodeID) {
		return 'S' + nodeID + '('+this.pin1value+','+this.pin2value+','+this.velocity+');';

		//todo: refactor arduino lib with separate nodes for 1 or 2 motors, confirm that this is already done
	},
	draw: function(ctx, x, y, radius) {
		drawText ( ctx, this.velocity, x-radius, y+radius, "#eeeeee" );
	}
}
