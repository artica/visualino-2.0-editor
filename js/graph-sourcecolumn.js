function SourceColumn() {
	
	this.sourcetype_selected = 0; // type of nodes displayed
	this.sourcetypes = [];
	var st = 0;
	
	this.sourcetypes[st++] = new SourceType([
								new NodeTypeRoot(),
								new NodeTypeJsonImport(),
								new NodeTypeParallel(),
								new NodeTypeSequence()
								],'#202093');
	this.sourcetypes[st++] = new SourceType([
								new NodeTypeDelayTime(),
								new NodeTypeStatusOverride(),
								new NodeTypeSensor()
								],'#707320');
	this.sourcetypes[st++] = new SourceType([
								new NodeTypeMotor(),
								new NodeTypeServoRange(),
//								new NodeTypeServoIncrementor(),
//								new NodeTypeServoThreshold(),
								new NodeTypeLED()
								],'#209320');
	this.sourcetypes[st++] = new SourceType([
								new NodeTypeVideo(),
								new NodeTypeImage()
								],'#932020');
		
	this.source_selected = -1; // source index being dragged
	
	// screen positioning 
	this.sources_height = 20;
	this.sources_top = 50;
	this.sources_vpad = 40;
	this.sources_hpad = 40;

	// optimization for calculating distance
	this.sources_radius = this.sources_height;
}

SourceColumn.prototype = {
	change_sourcetype: function(index) {
		this.sourcetype_selected = index;
		this.source_selected = -1;
	},
	unselect_source: function(index) {
		this.source_selected = -1;
	},
	draw: function(ctx) {
		for (var s = 0; s < this.sourcetypes[this.sourcetype_selected]['nodes'].length; s++) {	
				
			var thisx = this.sources_hpad;
			var thisy = this.sources_top + this.sources_vpad + s*(this.sources_height+this.sources_vpad);
			
			drawCircle( ctx, thisx, thisy, this.sources_height,
						this.sourcetypes[this.sourcetype_selected]['color'],
						this.sourcetypes[this.sourcetype_selected]['color']
					);
			
			drawText( ctx, t[lang][this.sourcetypes[this.sourcetype_selected]['nodes'][s].toString()],
						thisx, thisy - 8, "#ffffff" );
		}
	}
}
