
//
// create unique identifiers
//

function UID(last_uid) 
{
	this.last_uid = last_uid ? last_uid : 0;
}

UID.prototype = 
{
	get_uid: function() 
	{
		this.last_uid++;
		return this.last_uid.toString(16);
	},
	set_last_uid: function(string16) 
	{
		this.last_uid = parseInt(string16,16);
	}
}


// calculate the distance between two points
function distance_to_point(x1, y1, x2, y2) 
{
	var dx = x2 - x1;
	var dy = y2 - y1;
	return Math.sqrt(dx * dx + dy * dy);
}


// Square of a number
function sqr(x) { return x * x }
// Square of the distance between two points
function dist2(v, w) { return sqr(v.x - w.x) + sqr(v.y - w.y) }
// Distance between a point p and a line segment, defined by two points v and w
function distToSegment(p, v, w) 
{

    var l2 = dist2(v, w);
    if (l2 == 0) return Math.sqrt(dist2(p, v));
    var t = ((p.x - v.x) * (w.x - v.x) + (p.y - v.y) * (w.y - v.y)) / l2;
    if (t < 0) return Math.sqrt(dist2(p, v));
    if (t > 1) return Math.sqrt(dist2(p, w));
    return Math.sqrt(dist2(p, { x: v.x + t * (w.x - v.x),
                    y: v.y + t * (w.y - v.y) }));
}


//
// canvas drawing functions
//

function drawCircle(ctx, thisx, thisy, radius, fillStyle, strokeStyle) {
	ctx.fillStyle = fillStyle;
	ctx.strokeStyle = strokeStyle;
	ctx.beginPath();
	ctx.arc( thisx, thisy, radius, 0, Math.PI*2, true );
	ctx.fill();
	ctx.stroke();
	ctx.closePath();
}

function drawText( ctx, text, thisx, thisy, fillStyle ) {
	ctx.fillStyle=fillStyle;
	ctx.font="14px futura";
	ctx.textBaseline="top";
	ctx.fillText(text, thisx, thisy);
}

function padByte(value)
{
	var x = parseInt(value);
	var str = x.toString(16);
	var pad = "00";
	return pad.substring(0, pad.length - str.length) + str;
}

function padLong(value)
{
	console.log('padLong:'+value);
	var x = parseInt(value);
	var str = x.toString(16);
	console.log(str);
	var pad = "00000000";
	return pad.substring(0, pad.length - str.length) + str;
}


//
// clone object
//

var object_create = Object.create;
if (typeof object_create !== 'function') {
    object_create = function(o) {
        function F() {}
        F.prototype = o;
        return new F();
    };
}
function deepCopy(src, /* INTERNAL */ _visited) {
    if(src == null || typeof(src) !== 'object'){
        return src;
    }

    // Initialize the visited objects array if needed
    // This is used to detect cyclic references
    if (_visited == undefined){
        _visited = [];
    }
    // Otherwise, ensure src has not already been visited
    else {
        var i, len = _visited.length;
        for (i = 0; i < len; i++) {
            // If src was already visited, don't try to copy it, just return the reference
            if (src === _visited[i]) {
                return src;
            }
        }
    }

    // Add this object to the visited array
    _visited.push(src);

    //Honor native/custom clone methods
    if(typeof src.clone == 'function'){
        return src.clone(true);
    }

    //Special cases:
    //Date
    if (src instanceof Date){
        return new Date(src.getTime());
    }
    //RegExp
    if(src instanceof RegExp){
        return new RegExp(src);
    }
    //DOM Elements
    if(src.nodeType && typeof src.cloneNode == 'function'){
        return src.cloneNode(true);
    }

    //If we've reached here, we have a regular object, array, or function

    //make sure the returned object has the same prototype as the original
    var proto = (Object.getPrototypeOf ? Object.getPrototypeOf(src): src.__proto__);
    if (!proto) {
        proto = src.constructor.prototype; //this line would probably only be reached by very old browsers 
    }
    var ret = object_create(proto);

    for(var key in src){
        //Note: this does NOT preserve ES5 property attributes like 'writable', 'enumerable', etc.
        //For an example of how this could be modified to do so, see the singleMixin() function
        ret[key] = deepCopy(src[key], _visited);
    }
    return ret;
}
