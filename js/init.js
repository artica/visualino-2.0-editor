var shiftDown = false;
var ctrlDown = false;


window.onkeydown = function(e)
{
	//console.log(e.keyCode);
	switch(e.keyCode) 
	{
		case 46: // del key
			g.remove_selection();
		break;
		case 16: // shift key
			shiftDown = true;
		break;
		case 17: // control key
			ctrlDown = true;
		break;
		
		case 88: // X key
			g.reset_animation(1);
		break;
		case 67: // C key
			// CTRL-C combo
			if (ctrlDown)
			{
				g.copySelected();
			}
		break;
		case 86: // V key
			// CTRL-V combo
			if (ctrlDown)
			{
				g.pasteSelected();
			}
		break;

	}
};

window.onkeyup = function(e)
{
	switch(e.keyCode) 
	{
		case 16: // shift key
			shiftDown = false;
    		//g.clean_temporary_connection(); 
		break;
		case 17: // control key
			ctrlDown = false;
		break;
	}   
};

var c, ctx, cw, ch;
var g;
var animation_frame;

window.onload = function init() 
{
	//
	// create dom
	//
	var container = document.createElement('div');
	container.id = 'container';
	window.document.body.appendChild(container);

		var top = document.createElement('div');
		top.id = 'topcanvasdiv';
		container.appendChild(top);

			c = document.createElement('canvas');
			c.id = 'mycanvas';
			c.innerHTML = t[lang]['no_canvas'];
			top.appendChild(c);

		var propertiesdiv = document.createElement('div');
		propertiesdiv.id = 'propertiesdiv';
		container.appendChild(propertiesdiv);
	
			var infobox = document.createElement('div');
			infobox.id = 'infobox';
			infobox.innerHTML = t[lang]['infobox_default'];
			propertiesdiv.appendChild(infobox);
	
			var propertiesdisplaybox = document.createElement('div');
			propertiesdisplaybox.id = 'propertiesdisplaybox';
			propertiesdiv.appendChild(propertiesdisplaybox);
			
	//
	// init canvas, mouse listeners and graph
	//
	
	if (c && top) 
	{
		ctx = c.getContext('2d');
		cw = ctx.width = c.width = top.clientWidth;
		ch = ctx.height = c.height = top.clientHeight;
		
		//window.addEventListener("mousedown", mouseDown, false);
		$('#mycanvas').on('mousedown', mouseDown);
		$('#mycanvas').on('mouseup', mouseUp);
		$('#mycanvas').on('mousemove', mouseMove);
		
		$('#mycanvas').bind('mousewheel', zoomTree);
	
		g = new Graph(Node);
		var root = new NodeTypeRoot();
		root.set_color('#202093');
		var n1 = g.add_node('', root, 350, 150, 20);	
		
		g.clear_display();		
		
		animation_frame = requestAnimationFrame(animate);
	} else
	{
		alert(t[lang]['bad_canvas']);
	}
	
	window.ondragstart = function() { return false; }

	
	g.reset_graph();

	g.import_json(	'{"nodes":[{"name":"","type":{"name":"Root"},"uid":"2","x":642,"y":78,"radius":20},{"name":"","type":{"name":"Parallel","completion_policy":1,"failure_policy":0},"uid":"3","x":639,"y":174,"radius":20},{"name":"","type":{"name":"Status Override","failure_override":0,"running_override":1,"success_override":1},"uid":"4","x":383,"y":279,"radius":20},{"name":"","type":{"name":"Sequence"},"uid":"5","x":224,"y":468,"radius":20},{"name":"","type":{"name":"LED","pintype":"Digital","pinvalue":"0","value":"1"},"uid":"6","x":137,"y":592,"radius":20},{"name":"","type":{"name":"Delay Time","value":"200"},"uid":"7","x":188,"y":594,"radius":20},{"name":"","type":{"name":"LED","pintype":"Digital","pinvalue":"0","value":"0"},"uid":"8","x":254,"y":594,"radius":20},{"name":"","type":{"name":"Delay Time","value":"300"},"uid":"9","x":306,"y":591,"radius":20},{"name":"","type":{"name":"Status Override","failure_override":0,"running_override":1,"success_override":1},"uid":"a","x":781,"y":269,"radius":20},{"name":"","type":{"name":"Sequence"},"uid":"b","x":858,"y":393,"radius":20},{"name":"","type":{"name":"Sensor","pintype":0,"pinvalue":"3","threshold":640},"uid":"c","x":707,"y":483,"radius":20},{"name":"","type":{"name":"LED","pintype":"Digital","pinvalue":"1","value":"1"},"uid":"d","x":770,"y":488,"radius":20},{"name":"","type":{"name":"Delay Time","value":"888"},"uid":"e","x":838,"y":488,"radius":20},{"name":"","type":{"name":"LED","pintype":"Digital","pinvalue":"1","value":"0"},"uid":"f","x":915,"y":487,"radius":20},{"name":"","type":{"name":"Delay Time","value":"1500"},"uid":"10","x":977,"y":496,"radius":20},{"name":"","type":{"name":"Sequence"},"uid":"11","x":470,"y":468,"radius":20},{"name":"","type":{"name":"Delay Time","value":"400"},"uid":"12","x":442,"y":594,"radius":20},{"name":"","type":{"name":"Delay Time","value":"600"},"uid":"13","x":546,"y":596,"radius":20},{"name":"","type":{"name":"LED","pintype":"Digital","pinvalue":"2","value":"1"},"uid":"14","x":391,"y":600,"radius":20},{"name":"","type":{"name":"LED","pintype":"Digital","pinvalue":"2","value":"0"},"uid":"15","x":496,"y":598,"radius":20},{"name":"","type":{"name":"Sequence"},"uid":"16","x":370,"y":358,"radius":20}],"connections":["2:3","3:4","3:a","4:16","5:6","5:7","5:8","5:9","a:b","b:c","b:d","b:e","b:f","b:10","11:12","11:13","11:14","11:15","16:5","16:11"]}');
}

function zoomTree(e)
{
    e.preventDefault();
    if(e.originalEvent.wheelDelta /120 > 0) 
    {
        console.log('scrolling up !');
        g.zoom(0.1);
    }
    else
    {
        console.log('scrolling down !');
        g.zoom(-0.1);
    }
}

function startAnimation() 
{
	this.update_collision = true;
	this.update_animation = true;
	animate();
}

function animate() 
{
	cancelAnimationFrame(animation_frame);
	animation_frame = requestAnimationFrame(animate);
	g.draw(ctx);
}
