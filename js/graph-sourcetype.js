function SourceType(nodes, color) 
{	
	this.nodes = nodes;
	this.color = color;
	this.init_color();
}

SourceType.prototype = 
{
	init_color: function() 
	{
		for (var i=0; i<this.nodes.length; i++) 
		{
			this.nodes[i].set_color(this.color);
			//todo: affect brightness of each node by a certain value, 
			//so each node is slightly brighter/darker
		}
	}
}
